--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;

CREATE EXTENSION postgis;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: user_userrole_enum; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.user_userrole_enum AS ENUM (
    'ADMIN',
    'USER'
);


ALTER TYPE public.user_userrole_enum OWNER TO postgres;

SET default_tablespace = '';

--SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.author OWNER TO postgres;

--
-- Name: author_book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author_book (
    "authorId" integer NOT NULL,
    "bookId" integer NOT NULL
);


ALTER TABLE public.author_book OWNER TO postgres;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_id_seq OWNER TO postgres;

--
-- Name: author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.author_id_seq OWNED BY public.author.id;


--
-- Name: book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.book OWNER TO postgres;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_id_seq OWNER TO postgres;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_id_seq OWNED BY public.book.id;


--
-- Name: club; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.club (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying NOT NULL,
    "orgCode" character varying NOT NULL,
    rate integer NOT NULL,
    "rosterApi" character varying,
    "scheduleApi" character varying,
    "videoApi" character varying,
    "eventsApi" character varying,
    "newsApi" character varying,
    facebook character varying,
    instagram character varying,
    twitter character varying,
    "createdAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    "logoUrl" character varying
);


ALTER TABLE public.club OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: promotion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.promotion (
    "promoId" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "promoCodePrefix" character varying,
    "promoCodePoints" integer,
    validity character varying,
    "sponsorId" character varying,
    "orgId" character varying,
    "promoType" character varying,
    "pointsAvailable" integer,
    "promoTotalCount" integer,
    "noPointsPerPromo" integer,
    "promoMessage" character varying,
    "promoTitle" character varying,
    "promoSentToMail" character varying,
    "promoSentTOClub" character varying,
    "createdAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL
);


ALTER TABLE public.promotion OWNER TO postgres;

--
-- Name: sponsor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsor (
    sponsor_name character varying,
    point_request integer,
    contact_name character varying,
    contact_email character varying,
    contact_phone character varying,
    contact_address character varying,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public.sponsor OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    "givenName" character varying NOT NULL,
    email text NOT NULL,
    "phoneNumber" text NOT NULL,
    "encryptedMnemonic" character varying NOT NULL,
    "userRole" public.user_userrole_enum DEFAULT 'USER'::public.user_userrole_enum NOT NULL,
    permissions character varying NOT NULL,
    "isActive" boolean DEFAULT true NOT NULL,
    username character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT CURRENT_TIMESTAMP(6) NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: author id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author ALTER COLUMN id SET DEFAULT nextval('public.author_id_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book ALTER COLUMN id SET DEFAULT nextval('public.book_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: author_book PK_1afdf8d126ed119cdb9057da29d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT "PK_1afdf8d126ed119cdb9057da29d" PRIMARY KEY ("authorId", "bookId");


--
-- Name: promotion PK_2a3fe649fc6812eeff1da7afcb8; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promotion
    ADD CONSTRAINT "PK_2a3fe649fc6812eeff1da7afcb8" PRIMARY KEY ("promoId");


--
-- Name: sponsor PK_31c4354cde945c685aabe017541; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsor
    ADD CONSTRAINT "PK_31c4354cde945c685aabe017541" PRIMARY KEY (id);


--
-- Name: author PK_5a0e79799d372fe56f2f3fa6871; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT "PK_5a0e79799d372fe56f2f3fa6871" PRIMARY KEY (id);


--
-- Name: club PK_79282481e036a6e0b180afa38aa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.club
    ADD CONSTRAINT "PK_79282481e036a6e0b180afa38aa" PRIMARY KEY (id);


--
-- Name: book PK_a3afef72ec8f80e6e5c310b28a4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT "PK_a3afef72ec8f80e6e5c310b28a4" PRIMARY KEY (id);


--
-- Name: product PK_bebc9158e480b949565b4dc7a82; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: user UQ_f2578043e491921209f5dadd080; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_f2578043e491921209f5dadd080" UNIQUE ("phoneNumber");


--
-- Name: author_book FK_59f15c03bbf9fa2c992f26f5f75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT "FK_59f15c03bbf9fa2c992f26f5f75" FOREIGN KEY ("authorId") REFERENCES public.author(id) ON DELETE CASCADE;


--
-- Name: author_book FK_7781d00994265ab0a4e8d5eda75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT "FK_7781d00994265ab0a4e8d5eda75" FOREIGN KEY ("bookId") REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

