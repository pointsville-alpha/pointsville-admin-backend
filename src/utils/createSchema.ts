import { buildSchema } from "type-graphql";
import { UserResolver } from "../modules/user/GetUsers";
import { MeResolver } from "../modules/user/Me";
import { ProfilePictureResolver } from "../modules/user/ProfilePicture";
import { whoamiResolver } from "../modules/user/Register";
import { Container } from "typedi";
import { ClubResolver } from "../modules/club/ClubResolver";
import { handler } from "./decode-verify-jwt";
import { User } from "../entity/User";
import { FeedbackResolver } from "../modules/feedback/FeedbackResolver";
import { PromotionResolver } from '../modules/promotion/promotionResolver';
import { SponsorsResolver } from '../modules/sponsors/sponsorsResolver';
import { PlayRosterResolver } from '../modules/playersroster/PlayersRoastersResolver';
import { VideoStreamingResolver } from '../modules/video-streaming/VideoStreamingResolver';
import { EventUriResolver } from '../modules/eventuri/EventUriResolver';
import { GameScheduleResolver } from '../modules/gamescheduler/GameScheduleResolver';
import { UsertransactionsResolver } from "../modules/usertransactions/usertransactionResolver";
import { HuntResolver } from "../modules/hunt/HuntResolver";


export const createSchema = () =>
  buildSchema({
    container: Container,
    resolvers: [
      MeResolver,
      whoamiResolver,
      ProfilePictureResolver,
      UserResolver,
      ClubResolver,
      FeedbackResolver,
      PromotionResolver,
      SponsorsResolver,
      PlayRosterResolver,
      VideoStreamingResolver,
      EventUriResolver,
      GameScheduleResolver,
      UsertransactionsResolver,
      HuntResolver
    ],
    authChecker: async ({ context }, roles) => {
      console.log(roles);
      console.log(context.user);
      //Roles Check
      const auth = await handler({ token: context.req.headers.authorization });
      let u: any = await User.findOne({username: auth.userName});
      if (!!u && roles.length === 0) {
        return auth.isValid;
      } else if(!!u && roles.includes(u.userRole)) {
        return auth.isValid;
      } else {
        return false;
      }
    }
  });
