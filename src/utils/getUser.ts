import { handler } from "./decode-verify-jwt";

export const getUser = async (token: string) => {

    const auth = await handler({ token: token });

    if(!auth.isValid) return null;
    return auth.userName;
}