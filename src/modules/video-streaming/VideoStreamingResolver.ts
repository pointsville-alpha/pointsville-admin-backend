import { Arg, Authorized, Query, Resolver } from "type-graphql";
import { VideoStreaming } from "../../entity/VideoStreaming";

@Resolver()
export class VideoStreamingResolver {
    @Authorized()
    @Query(() => [VideoStreaming])
    async getVideoStreamingByOrgId(@Arg("orgCode", () => String) orgCode: string) {
        const videoStreaming = await VideoStreaming.find({where:{ orgCode: orgCode}});
        return videoStreaming;
    }
}