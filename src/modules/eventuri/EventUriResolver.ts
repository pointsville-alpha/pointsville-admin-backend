import { Arg, Authorized, Query, Resolver } from "type-graphql";
import { EventUri } from "../../entity/EventsUri";

@Resolver()
export class EventUriResolver {
    @Authorized()
    @Query(() => [EventUri])
    async getEventUriByOrgId(@Arg("orgcode", () => String) orgcode: string) {
        const eventUri = await EventUri.find({where:{ orgcode: orgcode }});
        return eventUri;
    }
}