import { Field, InputType, } from "type-graphql";

@InputType()
export class NewFeedbackInput {
    @Field()
    feedback: string;

    @Field()
    rating: number;

}


@InputType()
export class UpdateFeedbackInput {

    @Field({ nullable: true })
    feedback: string;

    @Field({ nullable: true })
    rating: number;
}