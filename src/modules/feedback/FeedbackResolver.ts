import { Arg, Authorized, Mutation, Query, Resolver, Int } from "type-graphql";
import { Feedback } from "../../entity/Feedback";
import { NewFeedbackInput, UpdateFeedbackInput } from "./FeedbackInput";

@Resolver()
export class FeedbackResolver {

    @Authorized()
    @Mutation(() => Feedback)
    async createFeedback(
        @Arg("data") data: NewFeedbackInput,
    ) {
        return await Feedback.create({
            ...data
        }).save();
    }

    @Authorized()
    @Mutation(() => Feedback)
    async updateFeedback(
        @Arg("id") id: string,
        @Arg("data") data: UpdateFeedbackInput
    ) {
        const feedback = await Feedback.findOne({ where: { id } });
        if (!feedback) throw new Error("feedback not found!");
        Object.assign(feedback, data);
        await feedback.save();
        return feedback;
    }

    @Authorized()
    @Mutation(() => Boolean)
    async deleteFeedback(@Arg("feedbackId", () => Int) feedbackId: string) {
      await Feedback.delete({ id: feedbackId });
      return true;
    }

    @Authorized()
    @Query(() => [Feedback])
    async getAllFeedbacks() {
        return Feedback.find({
            cache: true,
        });
    }

    @Authorized()
    @Query(() => Feedback)
    async getFeedbackById(@Arg("feedbackId", () => String) feedbackId: string) {
        const feedback = await Feedback.findOne({ id: feedbackId });
        return feedback;
    }
}

