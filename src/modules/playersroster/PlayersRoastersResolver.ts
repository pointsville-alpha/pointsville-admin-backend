import { Arg, Authorized, Query, Resolver } from "type-graphql";
import { PlayRoster } from "../../entity/PlayersRoster";

@Resolver()
export class PlayRosterResolver {
    @Authorized()
    @Query(() => [PlayRoster])
    async getPlayersByOrgId(@Arg("orgcode", () => String) orgcode: string) {
        const playRoster = await PlayRoster.find({where:{ orgcode: orgcode}});
        return playRoster;
    }
}