import { MiddlewareFn } from "type-graphql";

import { MyContext } from "../../types/MyContext";

export const isAuth: MiddlewareFn<MyContext> = async ({ context }, next) => {
  console.log("test", context)
  if (!context) {
    throw new Error("not authenticatded");
  }

  return next();
};
