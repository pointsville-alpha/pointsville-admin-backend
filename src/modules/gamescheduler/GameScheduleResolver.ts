import { Arg, Authorized, Query, Resolver } from "type-graphql";
import {  GameSchedule} from "../../entity/GamesSchedule";

@Resolver()
export class GameScheduleResolver {
    @Authorized()
    @Query(() => [GameSchedule])
    async getGameScheduleByOrgId(@Arg("orgcode", () => String) orgcode: string) {
        const gameSchedule = await GameSchedule.find({where:{ orgcode: orgcode }});
        return gameSchedule;
    }

    @Authorized()
        @Query(() => [GameSchedule])
        async getAllGameSchedule() {
        return GameSchedule.find();
    }
}
