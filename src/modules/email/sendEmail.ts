import nodemailer from "nodemailer";

export class Email {

 async sendEmail(TO: any, SUBJECT: any, BODY: any) {
    const SERVICE_EMAIL = 'noreplypointsville@gmail.com';
    const SERVICE_EMAIL_PASSWORD = 'Welcome@ta';
    const EMAIL_SERVICE_MIDDLEWARE = 'gmail';
    const transporter = nodemailer.createTransport({
        service: EMAIL_SERVICE_MIDDLEWARE,
        auth: {
            user: SERVICE_EMAIL,
            pass: SERVICE_EMAIL_PASSWORD
        }
    });
    const mailOptions = {
        to: TO,
        from: SERVICE_EMAIL,
        subject: SUBJECT,
        html: BODY
    };
    transporter.sendMail(mailOptions)
        .then((res) => {
            console.log("success",res)
        }).catch((err) => {
            console.log("failure",err)
        })
}
}
