import { Field, InputType,Int,Float } from "type-graphql";

@InputType()
export class NewClubInput {

  @Field()
  name: string;

  @Field()
  orgCode: string;

  @Field(() => Float,{ nullable: true })
  rate: number;

  @Field({ nullable: true })
  logoUrl?: string;

  @Field({ nullable: true })
  rosterApi?: string;

  @Field({ nullable: true })
  scheduleApi?: string;

  @Field({ nullable: true })
  videoApi?: string;

  @Field({ nullable: true })
  eventsApi?: string;

  @Field({ nullable: true })
  newsApi?: string;

  @Field({ nullable: true })
  facebook?: string;

  @Field({ nullable: true })
  instagram?: string;

  @Field({ nullable: true })
  twitter?: string;

  @Field(() => Int,{nullable: true })
  fanCount?: number;

  @Field({nullable: true })
  isActive?:boolean

  @Field(() => Int,{nullable: true })
  distributionPoints?: number;

  @Field(() => Int,{nullable: true })
  availablePoints?: number;

  @Field(() => Int,{nullable: true })
  totalPoints?: number;
}


@InputType()
export class UpdateClubInput {

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  orgCode?: string;

  @Field(() => Float,{ nullable: true })
  rate?: number;

  @Field({ nullable: true })
  logoUrl?: string;

  @Field({ nullable: true })
  rosterApi?: string;

  @Field({ nullable: true })
  scheduleApi?: string;

  @Field({ nullable: true })
  videoApi?: string;

  @Field({ nullable: true })
  eventsApi?: string;

  @Field({ nullable: true })
  newsApi?: string;

  @Field({ nullable: true })
  facebook?: string;

  @Field({ nullable: true })
  instagram?: string;

  @Field({ nullable: true })
  twitter?: string;

  @Field(() => Int,{nullable: true })
  fanCount?: number;

  @Field({nullable: true })
  isActive?:boolean

  @Field(() => Int,{nullable: true })
  distributionPoints?: number;

  @Field(() => Int,{nullable: true })
  availablePoints?: number;

  @Field(() => Int,{nullable: true })
  totalPoints?: number;
  
}