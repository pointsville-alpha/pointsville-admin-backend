import  { Resolver, Query, Mutation, Arg, Int,Float, Field, Authorized,ObjectType} from 'type-graphql';
import { getConnection, getRepository } from "typeorm";
import { Club } from "../../entity/Club";
import { NewClubInput, UpdateClubInput } from "./ClubInput";
import { Promotion } from "../../entity/Promotion";
//import { User } from "../../entity/User";

@Resolver()
export class ClubResolver {
  @Authorized("ADMIN")
  @Mutation(() => Club)
  async createOrg(
    @Arg("data") data: NewClubInput,
  ) {
    await getConnection().queryResultCache!.remove(["club:find"]);
    const org = await Club.find({
      name: data.name
    });


    if (!org.length) {
      return await Club.create({
        ...data
      }).save();
    } else {
      const error = new Error("Club already exists");
      throw error;
    }
  }

  @Authorized("ADMIN")
  @Mutation(() => Club)
  async updateOrg(
    @Arg("id") id: string,
    @Arg("data") data: UpdateClubInput
  ) {
    const club = await Club.findOne({ where: { id } });
    if (!club) throw new Error("Club not found!");
      Object.assign(club, data);
      await club.save();
      return club;
  }

  // @Mutation(() => Author)
  // async createAuthor(@Arg("name") name: string) {
  //   return Author.create({ name }).save();
  // }

  // @Mutation(() => Boolean)
  // async addAuthorBook(
  //   @Arg("authorId", () => Int) authorId: number,
  //   @Arg("bookId", () => Int) bookId: number
  // ) {
  //   await AuthorBook.create({ authorId, bookId }).save();
  //   return true;
  // }

  // @Mutation(() => Boolean)
  // async deleteBook(@Arg("bookId", () => Int) bookId: number) {
  //   // await AuthorBook.delete({ bookId });
  //   await Book.delete({ id: bookId });
  //   return true;
  // }

  @Authorized()
  @Query(() => [clubDetails])
  async orgs() {
    const clubs =  await Club.find({where : { isActive : true}});

      const clubDetails:any = clubs.map(async(clubDetail:any) => {
        const clubswithusers: any = await getRepository(Club)
          .createQueryBuilder("club")
          .leftJoinAndSelect("club.users", "user")
          .where({id: clubDetail.id})
          .getOne();
        return{
          id : clubDetail.id,
          name : clubDetail.name,
          rate : clubDetail.rate,
          orgCode : clubDetail.orgCode,
          logoUrl : clubDetail.logoUrl,
          availablePoints : clubDetail.availablePoints,
          totalPoints : clubDetail.totalPoints,
          distributionPoints : clubDetail.distributionPoints,
          createdAt : clubDetail.createdAt,
          fanCount : clubswithusers.users.length,
          isActive:clubDetail.isActive
        }
    })
    return clubDetails
   
  }
  @Authorized()
  @Query(() => [clubDetails])
  async orgsInActive() {
    const clubs =  await Club.find({where : { isActive : false}});

      const clubDetails:any = clubs.map(async(clubDetail:any) => {
        const clubswithusers: any = await getRepository(Club)
          .createQueryBuilder("club")
          .leftJoinAndSelect("club.users", "user")
          .where({id: clubDetail.id})
          .getOne();
        return{
          id : clubDetail.id,
          name : clubDetail.name,
          rate : clubDetail.rate,
          orgCode : clubDetail.orgCode,
          logoUrl : clubDetail.logoUrl,
          availablePoints : clubDetail.availablePoints,
          totalPoints : clubDetail.totalPoints,
          distributionPoints : clubDetail.distributionPoints,
          createdAt : clubDetail.createdAt,
          fanCount : clubswithusers.users.length,
          isActive:clubDetail.isActive
        }
    })
    return clubDetails
   
  }
  @Authorized()
  @Query(() => [clubDetails])
  async orgsAll() {
    const clubs =  await Club.find();

      const clubDetails:any = clubs.map(async(clubDetail:any) => {
        const clubswithusers: any = await getRepository(Club)
          .createQueryBuilder("club")
          .leftJoinAndSelect("club.users", "user")
          .where({id: clubDetail.id})
          .getOne();
        return{
          id : clubDetail.id,
          name : clubDetail.name,
          rate : clubDetail.rate,
          orgCode : clubDetail.orgCode,
          logoUrl : clubDetail.logoUrl,
          availablePoints : clubDetail.availablePoints,
          totalPoints : clubDetail.totalPoints,
          distributionPoints : clubDetail.distributionPoints,
          createdAt : clubDetail.createdAt,
          fanCount : clubswithusers.users.length,
          isActive:clubDetail.isActive
        }
    })
    return clubDetails
   
  }
  @Authorized()
  @Query(() => clubDetails!, { nullable: false })
  async getClub(@Arg("clubId", () => String) ClubId: string) {
    const club: any = await Club.findOne({ id: ClubId },{relations: ["users"]});
    const clubswithusers: any = await getRepository(Club)
    .createQueryBuilder("club")
    .leftJoinAndSelect("club.users", "user")
    .where({id: club.id})
    .getOne();
    const clubDetails = {
        id : club.id,
        name : club.name,
        orgCode: club.orgCode,
        logoUrl: club.logoUrl,
        rate: club.rate,
        availablePoints : club.availablePoints,
        totalPoints : club.totalPoints,
        distributionPoints : club.distributionPoints,
        rosterApi: club.rosterApi,
        scheduleApi: club.scheduleApi,
        videoApi: club.videoApi,
        eventsApi: club.eventsApi,
        newsApi: club.newsApi,
        facebook: club.facebook,
        instagram: club.instagram,
        twitter: club.twitter,
        createdAt : club.updatedAt,
        isActive : club.isActive,
        fanCount: clubswithusers.users.length,

    }

    //club.fanCount = clubswithusers.users.length;
    return JSON.parse(JSON.stringify(clubDetails));
  }


  @Authorized()
  @Query(() => Club!, { nullable: false },)
  async getClubWithPromos(@Arg("clubId", () => String) ClubId: string) {
    const club: any = await Club.findOne({ id: ClubId });
    //console.log(club);
    const promotion:any = await Promotion.find({where:{orgId: club.id}});
    club.promotionList = promotion;
    //console.log(club);
    return JSON.parse(JSON.stringify(club));
  }

  @Authorized("ADMIN")
  @Query(() => [clubDetails], { nullable: false },)
  async getPointsHolders(@Arg("clubId", () => String) ClubId: string) {
    const club: any = await Club.findOne({ id: ClubId },{relations: ["users"]});
    const clubswithusers: any = await getRepository(Club)
    .createQueryBuilder("club")
    .leftJoinAndSelect("club.users", "user")
    .where({id: club.id})
    .getOne();

    const userList = await clubswithusers.users.map( (user:any) => {
        return{
          username:user.username,
          givenName:user.givenName,
          phoneNumber:user.phoneNumber,
          profilePicture:user.profilePicture,
          email:user.email,
          walletId:user.walletId,
          walletBalance:user.walletBalance,
          createdAt:user.createdAt
        }
    })
    console.log(userList);
    //console.log(clubswithusers.users[0].givenName);
    return userList;
  }

}



@ObjectType()
class clubDetails {
    @Field()
    id?: string;

    @Field(() => String!, {nullable: true})
    name ?: string;
    
    @Field(() => String!, {nullable: true})
    logoUrl?: string;

    @Field(() => Float,{nullable : true})
    rate ?: number;

    @Field(() => Int,{nullable : true})
    totalPoints ?: number;
    
    @Field(() => Int,{nullable : true})
    availablePoints ?: number;
    
    @Field(() => Int,{nullable : true})
    distributionPoints ?: number;
    
    @Field(() => String!, {nullable: true})
    orgCode ?: string;

    @Field(() => Int!,{nullable : true})
    fanCount ?: number;

    @Field()
    isActive?:boolean

    @Field()
    createdAt :string;
  
    @Field(() => String!, {nullable: true})
    rosterApi?: string;
  
    @Field(() => String!, {nullable: true})
    scheduleApi?: string;
  
    @Field(() => String!, {nullable: true})
    videoApi?: string;
  
    @Field(() => String!, {nullable: true})
    eventsApi?: string;
  
    @Field(() => String!, {nullable: true})
    newsApi?: string;
  
    @Field(() => String!, {nullable: true})
    facebook?: string;
  
    @Field(() => String!, {nullable: true})
    instagram?: string;
  
    @Field(() => String!, {nullable: true})
    twitter?: string;



    @Field(() => String!, {nullable: true})
    givenName?: string;
  
    @Field(() => String!, {nullable: true})
    profilePicture?: string;


    @Field(() => String!, {nullable: true})
    email?: string;

    @Field(() => String!, {nullable: true})
    phoneNumber?: string;


    @Field(() => Float!, {nullable: true})
    walletId?: number;


    @Field(() => Int!, {nullable: true})
    walletBalance?: number;
  
    @Field(() => String!, {nullable: true})
    username?: string;

}
