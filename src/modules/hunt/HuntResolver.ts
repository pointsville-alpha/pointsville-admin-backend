import { Hunt } from "../../entity/Hunt";
import { Arg, Authorized, Ctx, Field, Float, InputType, Int, Mutation, ObjectType, Query, Resolver } from "type-graphql";
import { Point } from "../../entity/Point";
import { getConnection, getManager, getRepository } from "typeorm";
import { User } from "../../entity/User";
import { UserTransactions } from "../../entity/UserTransactions";
import { Club } from "../../entity/Club";
import { Sponsor } from "../../entity/Sponsor";

@InputType()
abstract class point {
    @Field(() => Number)
    pointsValue: number;

    @Field(() => Number)
    lat: number;

    @Field(() => Number)
    lng: number;
}

@InputType()
class NewHuntInput {
    @Field({ nullable: true })
    huntCodePrefix?: string;

    @Field(() => Int, { nullable: true })
    huntCodePoints?: number;

    @Field({ nullable: true })
    validity?: string;

    @Field(() => Int, { nullable: true })
    validityDay?: number;

    @Field({ nullable: true })
    sponsorId?: string;

    @Field({ nullable: true })
    orgId?: string;

    @Field(() => Int, { nullable: true })
    huntTotalCount?: number;

    @Field(() => Int, { nullable: true })
    noPointsPerHunt?: number;

    @Field(() => Float)
    mainLat: number

    @Field(() => Float)
    mainLng: number

    @Field({ nullable: true })
    locationName?: string;

    @Field(() => [point])
    points: point[];

}


@Resolver()
export class HuntResolver {

    @Authorized("ADMIN")
    @Mutation(() => Hunt)
    async createHunt(@Arg("data") data: NewHuntInput) {
        console.log(data);
        const newHunt = {
            huntCodePrefix: data.huntCodePrefix,
            huntCodePoints: data.huntCodePoints,
            huntTotalCount: data.huntTotalCount,
            pointsAvailable: data.huntTotalCount,
            noPointsPerHunt: data.noPointsPerHunt,
            validity: data.validity,
            validityDay: data.validityDay,
            sponsorId: data.sponsorId,
            orgId: data.orgId,
            mainLat: data.mainLat,
            mainLng: data.mainLng,
            locationName: data.locationName,
            location: `(${data.mainLat},${data.mainLng})`,
            isActive: true
        }

        const hunt: any = await Hunt.create({ ...newHunt }).save();

        const points: any = data.points;

        points.map(async (point: point) => {
            await Point.create({ ...point, hunt: hunt.id, location: `(${point.lat},${point.lng})` }).save()
        })

        return hunt;
    }

    @Authorized()
    @Query(() => [Hunt], { nullable: true })
    async getAllHuntsByOrg(@Arg("orgId", () => String) orgId: string) {
        const hunts = await Hunt.find({ relations: ["points"], where: { orgId: orgId }, skip: 0, take: 10, order: { createdAt: 'DESC' } });

        return hunts;

    }

    @Authorized()
    @Query(() => Hunt, { nullable: true })
    async getHunt(@Arg("id", () => String) id: string) {
        const hunt = await Hunt.findOne({ relations: ["points"], where: { id: id } });
        return hunt;
    }

    @Authorized()
    @Query(() => [Coins]!, { nullable: true })
    async getCoins(@Arg("location", () => String) location: string, @Arg("radius", () => Int) radius: number) {

        const latlng = location;

        const coins: any[] = await getManager().query(`SELECT * FROM point WHERE ST_Distance(ST_SetSRID(ST_MakePoint(lat, lng), 4326), ST_SetSRID(ST_MakePoint(${latlng}), 4326))*100 < ${radius}`);

        // const user = await User.findOne({where: {username: ctx.user}})

        let hunt: any, club: any, sponsor: any;

        let nearbyPoints = coins.map(async (coin) => {

            const linkedCoin = await getConnection()
                .createQueryBuilder()
                .relation(Point, "users")
                .of(coin) // you can use just post id as well
                .loadOne();

            if (!hunt) {

                hunt = await Hunt.findOne({ where: { id: coin.huntId } }).then((result) => result);
                console.log(hunt.id);
                if(hunt.pointsAvailable === 0 || !hunt.isActive) {
                    // return null;
                }
                if (hunt.orgId && hunt.sponsorId) {
                    club = await Club.findOne({ where: { id: hunt.orgId } });
                    sponsor = await Sponsor.findOne({ where: { id: hunt.sponsorId } });
                    coin.orgName = club.name;
                    coin.orgId = club.id;
                    coin.orgLogo = club.logoUrl;
                    coin.sponsorId = sponsor.id;
                    coin.sponsorLogo = sponsor.logoUrl;
                }
                coin.huntId = hunt.id;
            } else {
                coin.orgName = club?.name;
                coin.orgId = club?.id;
                coin.orgLogo = club?.logoUrl;
                coin.sponsorId = sponsor?.id;
                coin.sponsorLogo = sponsor?.logoUrl;
                coin.huntId = hunt?.id;
            }


            if (linkedCoin) {
                coin.isCollected = true;
            } else {
                coin.isCollected = false;
            }

            return await coin;

        })

        return nearbyPoints;

    }

    @Authorized()
    @Mutation(() => Boolean)
    async collectCoin(@Arg("coinId", () => String) coinId: string, @Ctx() ctx: any) {


        const coinswithusers: any = await getRepository(Point)
            .createQueryBuilder("point")
            .leftJoinAndSelect("point.users", "user")
            .leftJoinAndSelect("point.hunt", "hunt")
            .where({ id: coinId })
            .getOne();

        const user = await User.findOne({ where: { username: ctx.user } })

        if (coinswithusers.users.length === 0) {

            const hunt: any = await Hunt.findOne({ where: { id: coinswithusers?.hunt?.id } });

            if (hunt.pointsAvailable >= coinswithusers.pointsValue) {

                const transactionData = {
                    username: ctx.user,
                    orgId: hunt.orgId,
                    sponsorId: hunt.sponsorId,
                    promoId: hunt.id,
                    promoCode: hunt.huntCode,
                    points: coinswithusers.pointsValue,
                    transactionType: "HUNT"
                };

                await getConnection()
                    .createQueryBuilder()
                    .relation(User, "points")
                    .of(user)
                    .add(coinswithusers)
                    .then(() => { return true })
                    .catch(() => { return true })

                await UserTransactions.create(transactionData).save();
                await getConnection()
                    .createQueryBuilder()
                    .update(Hunt)
                    .set({
                        pointsAvailable: () => `"pointsAvailable" - ${coinswithusers.pointsValue}`
                    })
                    .where("id = :id", { id: coinswithusers?.hunt?.id })
                    .execute();

                console.log(coinswithusers);

                return true;
            } else if (hunt.pointsAvailable === 0) {
                await getConnection()
                    .createQueryBuilder()
                    .update(Hunt)
                    .set({
                        isActive: false
                    })
                    .where("id = :id", { id: coinswithusers?.hunt?.id })
                    .execute();
            }
        }

        return false;

    }



}

@ObjectType()
class Coins {

    @Field({ nullable: true })
    id: string;

    @Field({ nullable: true })
    isCollected: boolean;

    @Field({ nullable: true })
    lat: Number;

    @Field({ nullable: true })
    lng: Number;

    @Field({ nullable: true })
    pointsValue: number;

    @Field(() => String, { nullable: true })
    orgName: string;

    @Field(() => String, { nullable: true })
    orgId: string;

    @Field(() => String, { nullable: true })
    orgLogo: string;

    @Field(() => String, { nullable: true })
    sponsorId: string;

    @Field(() => String, { nullable: true })
    sponsorLogo: string;

    @Field({ nullable: true })
    huntId: string;
}
