import  { Resolver, Query, Mutation, Arg, Int, InputType, Field, Authorized,ObjectType} from 'type-graphql';
import {Sponsor} from '../../entity/Sponsor';
import { Club} from '../../entity/Club';
// import Axios from 'axios';

@InputType()
class NewSponsorInput{
    @Field()
    sponsorName : string;
    
    @Field(() => Int)
    pointRequest : number;
    
    @Field({ nullable: true })
    sponsorLogo?: string;

    @Field()
    contactName : string;

    @Field()
    contactEmail : string;

    @Field()
    contactPhone : string;

    @Field()
    contactAddress : string;

    @Field()
    clubId:string;



}

@InputType()
class UpdateSponsorInput{
    @Field({nullable : true})
    sponsorName ?: string;
    
    @Field({ nullable: true })
    sponsorLogo?: string;

    @Field(() => Int,{nullable : true})
    pointRequest ?: number;
    
    @Field({nullable : true})
    contactName ?: string;

    @Field({nullable : true})
    contactEmail ?: string;

    @Field({nullable : true})
    contactPhone ?: string;

    @Field({nullable : true})
    contactAddress ?: string;

    @Field()
    clubId :string;
}

@Resolver()
export class SponsorsResolver{ 
   //Create
   @Authorized()
   @Mutation( () => Sponsor)
    async createSponsor(
        @Arg("data", () => NewSponsorInput) data:NewSponsorInput
        ){
        const sponsor = await Sponsor.create({...data,availablePoints:data.pointRequest}).save();
        const club = await Club.findOne({where: {id: data.clubId}}); 
        if(club){
            const clubTotalPoint = club.totalPoints + data.pointRequest;
            const clubAvailPoint= club.availablePoints + data.pointRequest;
            const clubUpdatedDetails = {totalPoints : clubTotalPoint,availablePoints : clubAvailPoint} 
            Object.assign(club,clubUpdatedDetails);
            await club.save();   
        }
        // Block Chain Call
        // const blockChainData = await Axios.post('http://54.87.215.64:5000/createasset', {"assetamount" : 1, "reissuanceamount": 0})
        // console.log(blockChainData);
        return sponsor;
    }

    // Update
    @Authorized()
    @Mutation( () => Sponsor)
    async updateSponsor(
        @Arg("id",() => String) id:string,
        @Arg("data") data: UpdateSponsorInput
    ){
        const updatedSponsor = await Sponsor.findOne({ where: { id } });
        if (!updatedSponsor) throw new Error("Club not found!");
        Object.assign(updatedSponsor, data);
        await updatedSponsor.save();
        return updatedSponsor;
    }

    //Delete
    @Authorized()
    @Mutation (() => Boolean)
    async deleteSponsor(
        @Arg("id", () => String) id:string
    ){
        await Sponsor.delete({id});
        return true;
    }

    //View
   @Authorized()
    @Query( () => [SponsorDetails])
    async getsponsor(){
        //return Sponsor.find();
        const sponsor = await Sponsor.find();
        const sponsorDetails:any = sponsor.map(async(sponsorDetail:any) => {
            return{
                id:sponsorDetail.id,
                sponsorName:sponsorDetail.sponsorName,
                sponsorLogo:sponsorDetail.sponsorLogo,
                pointRequest:sponsorDetail.pointRequest,
                contactName :sponsorDetail.contactName,
                contactEmail:sponsorDetail.contactEmail,
                contactPhone:sponsorDetail.contactPhone,
                contactAddress:sponsorDetail.contactAddress,
                promotions:sponsorDetail.promotions,
                availablePoints:sponsorDetail.availablePoints,
                since : sponsorDetail.createdAt
            }
        })
        //console.log(sponsorDetails);
        return sponsorDetails;
    }

    @Authorized()
    @Query(() => Sponsor)
    async getSponsorWithId(@Arg("id", () => String) id: string) {
        const sponsor = await Sponsor.findOne({ id: id });
        return sponsor;
    }


    @Authorized()
    @Query (() => [SponsorDetails])
    async getSponsorWithOrgId(
        @Arg("clubId", () => String) clubId:string
    ){
       const sponsor = await Sponsor.find({where: {clubId:clubId}});
       const sponsorDetails:any = sponsor.map(async(sponsorDetail:any) => {
            return{
                id:sponsorDetail.id,
                sponsorName:sponsorDetail.sponsorName,
                sponsorLogo:sponsorDetail.sponsorLogo,
                pointRequest:sponsorDetail.pointRequest,
                contactName :sponsorDetail.contactName,
                contactEmail:sponsorDetail.contactEmail,
                contactPhone:sponsorDetail.contactPhone,
                contactAddress:sponsorDetail.contactAddress,
                promotions:sponsorDetail.promotions,
                availablePoints:sponsorDetail.availablePoints,
                since : sponsorDetail.createdAt
            }
        })
        //console.log(sponsorDetails);
        return sponsorDetails; 
    }

    // //Viewwith Single 
    // @Authorized()
    // @Query( () => Sponsor)
    //  async getSponsorWithId(
    //     @Arg("id",() => String) id:string
    //  ){
    //     const sponsorWithId = await Sponsor.find(({id : id }));
    //     if (!sponsorWithId) {
    //         throw new Error("Invalid recipe ID");
    //       }
    //       return sponsorWithId;
    // }
} 

@ObjectType()
class SponsorDetails {
    @Field()
    id: string;

    @Field(() => String!, {nullable: true})
    sponsorName ?: string;
    
    @Field(() => String!, {nullable: true})
    sponsorLogo?: string;

    @Field(() => Int,{nullable : true})
    pointRequest ?: number;

    @Field(() => Int,{nullable : true})
    promotions ?: number;
    
    @Field(() => Int,{nullable : true})
    availablePoints ?: number;
    

    @Field(() => String!, {nullable: true})
    contactName ?: string;

    @Field(() => String!, {nullable: true})
    contactEmail ?: string;

    @Field(() => String!, {nullable: true})
    contactPhone ?: string;

    @Field(() => String!, {nullable: true})
    contactAddress ?: string;

    @Field()
    clubId :string;
   
    @Field()
    since :string;

}