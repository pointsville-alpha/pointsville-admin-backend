
//var notify = require('./firebaseConfig')
import * as admin from "firebase-admin";
var serviceAccount = require("./pointsville-firebase-adminsdk-fz6qz-bafa00bd66.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://pointsville.firebaseio.com"
});

export async function postNotification(data: any) {
    if (data.deviceId) {
        const registrationToken = data.deviceId
        const message = data.message
        const title = data.title
        const payload = {
            'notification': {
                'title': title,
                'body': message,
            },
            'data': {
                'personSent': registrationToken
            }
        };
        const notification_options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };
        const options = notification_options
        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then((res: any) => {
                console.log("Sent", res)

            })
            .catch((error: any) => {
                console.log("Error", error)

            });
    } else {
        console.log("Notification Pending")
    }
}
