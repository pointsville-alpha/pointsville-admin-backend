import { UserTransactions } from "../../entity/UserTransactions";
import { Arg, Authorized, Ctx, Field, ObjectType, Query, Resolver ,Float ,Int, Mutation} from "type-graphql";
import { User } from "../../entity/User";
import { createQueryBuilder,getConnection } from "typeorm";
import { Club } from "../../entity/Club";
import { Sponsor } from "../../entity/Sponsor";
import {Promotion} from "../../entity/Promotion";
import { postNotification } from '../firebase/firebase';



@Resolver()
export class UsertransactionsResolver{ 

    @Authorized("ADMIN")
    @Query(() => [Trans])
     async getAllTransactions(){
        const userTrans = await createQueryBuilder(UserTransactions)
        .take(20)
        .orderBy("UserTransactions.createdAt", "DESC")
        .getMany();
        console.log(userTrans);
        const trans = userTrans.map(async (transaction: any) => {

            const user = await User.findOne({where: {username: transaction.username}});
            const org = await Club.findOne({where: {id: transaction.orgId}});
            const sponsor = await Sponsor.findOne({where: {id: transaction.sponsorId}});
            return {
                id: transaction.id,
                username: user?.givenName,
                userProfilePic: user?.profilePicture,
                walletId:user?.walletId,
                orgName: org?.name,
                orgLogo: org?.logoUrl,
                sponsorName: sponsor?.sponsorName,
                sponsorLogo: sponsor?.sponsorLogo,
                transactionDate: transaction.createdAt,
                transactionType:transaction.transactionType,
                points: transaction.points,
                price : transaction.price
            }

        })

        return trans;
    }

    @Authorized()
    @Query(() => [Trans])
     async getTransactionsByUser(@Ctx() ctx: any){
        const userTrans = await createQueryBuilder(UserTransactions)
        .where("UserTransactions.username = :username", {username: ctx.user})
        .take(20)
        .orderBy("UserTransactions.createdAt", "DESC")
        .getMany();

        const trans = userTrans.map(async (transaction: any) => {

            const user = await User.findOne({where: {username: transaction.username}});
            const org = await Club.findOne({where: {id: transaction.orgId}});
            const sponsor = await Sponsor.findOne({where: {id: transaction.sponsorId}});

            return {
                id: transaction.id,
                username: user?.givenName,
                userProfilePic: user?.profilePicture,
                orgName: org?.name,
                orgLogo: org?.logoUrl,
                sponsorName: sponsor?.sponsorName,
                sponsorLogo: sponsor?.sponsorLogo,
                transactionDate: transaction.createdAt,
                points: transaction.points
            }

        })

        return trans;
    }

    @Authorized("ADMIN")
    @Query(() => [Trans])
     async getTransactionsByClub(@Arg("orgId", () => String) orgId:string) {
        const userTrans = await createQueryBuilder(UserTransactions)
        .where("UserTransactions.orgId = :orgId", {orgId: orgId})
        .take(20)
        .orderBy("UserTransactions.createdAt", "DESC")
        .getMany();

        const trans = userTrans.map(async (transaction: any) => {

            const user = await User.findOne({where: {username: transaction.username}});
            const org = await Club.findOne({where: {id: transaction.orgId}});
            const sponsor = await Sponsor.findOne({where: {id: transaction.sponsorId}});

            return {
                id: transaction.id,
                username: user?.givenName,
                userProfilePic: user?.profilePicture,
                orgName: org?.name,
                orgLogo: org?.logoUrl,
                sponsorName: sponsor?.sponsorName,
                sponsorLogo: sponsor?.sponsorLogo,
                transactionDate: transaction.createdAt,
                points: transaction.points
            }
        })
        return trans;
    }
 
        @Authorized()
        @Query(() => [Trans])
            async getNotAvailedPromoByUser(@Arg("orgId", () => String) orgId: string,
            @Ctx() ctx: any
            ){
            const transactionData = await UserTransactions.find({where :{orgId: orgId,username:ctx.user}})
            const pro = await Promotion.find({where:{orgId: orgId}});
            let result=[]
            for(let i=0;i<=pro.length-1;i++){
                let pos = transactionData.findIndex(el=> el.promoCode == pro[i]["promoCode"]);
                const sponsor = await Sponsor.findOne({where : {id:pro[i].sponsorId} })
                if(pos == -1){
                    let promoObj = {
                        id:pro[i].id,
                        sponsorLogo: pro[i].sponsorLogo,
                        sponsorName: sponsor?.sponsorName,
                        validity:pro[i].validity,
                        createdAt:pro[i].createdAt,
                        points:pro[i].promoCodePoints, 
                        promoCode: pro[i].promoCode
                    }
                    result.push(promoObj)
                }
            }
            console.log("result",result)
            return result;

    }  

    @Authorized()
    @Query(() => [Trans])
     async getAvailedPromoByUser(@Arg("orgId", () => String) orgId: string,
     @Ctx() ctx: any
     ){
        const promoData = await UserTransactions.find({where: {username:ctx.user,orgId:orgId,transactionType:"PROMO"}});
        const promos = promoData.map(async (promo: any) => {
               const promoValues = await Promotion.findOne({where: {promoCode: promo.promoCode,orgId:orgId}});
               const sponsor = await Sponsor.findOne({where: {id: promo.sponsorId}});

               return {
                   id:promo.id,
                   sponsorLogo: promoValues?.sponsorLogo,
                   sponsorName: sponsor?.sponsorName,
                   validity:promoValues?.validity,
                   createdAt:promoValues?.createdAt,
                   price:promo.price,
                   points:promo.points,
                   promoCode: promoValues?.promoCode
               }
        })
        return promos
    }  

    @Authorized()
    @Query(() => [Trans])
     async getTransactionsGroupByOrg(
         @Ctx() ctx: any
         ){
        const groupedTrans = await createQueryBuilder(UserTransactions)
                .select("UserTransactions.orgId")
                .addSelect("SUM(UserTransactions.points)", "sum")
                .where("UserTransactions.username = :username", {username:ctx.user})
                .groupBy("UserTransactions.orgId")
                .getRawMany(); // need to check getMany
                console.log(groupedTrans);
        const groupByOrg = groupedTrans.map(async (transaction: any) => {
            const org = await Club.findOne({where: {id: transaction.UserTransactions_orgId}});
            const orgRate = org?.rate || 0;
            return {
                orgId : org?.id,
                orgName: org?.name,
                orgLogo: org?.logoUrl,
                orgRate: orgRate ,
                price:  transaction.sum * orgRate ,
                points: transaction.sum
            }
        })
        return groupByOrg;
    }

     @Authorized()
     @Query(() => Wallet)
     async walletSummary(
         @Ctx() ctx: any
         ){
            //ctx.user = "0969ff5e-68d3-4919-8385-78035e3718f1";
        const {point,price} = await createQueryBuilder(UserTransactions)
                .select("SUM(UserTransactions.points)", "point")
                .addSelect("SUM(UserTransactions.price)","price")
                .where("UserTransactions.username = :username", {username: ctx.user})
                .getRawOne();
        const {promoPoints} = await createQueryBuilder(UserTransactions)
                .select("SUM(UserTransactions.points)", "promoPoints")
                .where("UserTransactions.username = :username and UserTransactions.transactionType = :transactiontype", {username: ctx.user,transactiontype:"PROMO"})
                .getRawOne();
                console.log(promoPoints);
        const {receivedPoints} = await createQueryBuilder(UserTransactions)
                .select("SUM(UserTransactions.points)", "promoPoints")
                .where("UserTransactions.username = :username and UserTransactions.transactionType = :transactiontype", {username: ctx.user,transactiontype:"RECEIVED"})
                .getRawOne();
                console.log(promoPoints);
                
        const user = await User.findOne({where: {username: ctx.user}});

        const walletsummary =  {
            totalPoints :point || 0,
            totalAmount : price || 0,
            hunt : 0,
            promo : promoPoints || 0,
            received :receivedPoints || 0,
            walletId:user?.walletId
        }
        return walletsummary;
    }



    // User transfer point api
    @Authorized()
    @Mutation(() => Boolean)
    async pointTransferToUser(
        @Arg("walletId", () => Float) walletId:number,
        @Arg("orgId", () => String) orgId:string,
        @Arg("transferPoints", () => Int) transferPoints:number,
        @Ctx() ctx: any
    ){
        const fromUser = await User.findOne({where : {username : ctx.user}});
        const toUser = await User.findOne({where : {walletId:walletId}});
        const club  = await Club.findOne({where : {id:orgId}});
        const clubRate = club?.rate || 0;
        console.log(fromUser?.walletId);
        const {userTotalPoints,userTotalPrice} = await createQueryBuilder(UserTransactions)
                .select("SUM(UserTransactions.points)", "userTotalPoints")
                .addSelect("SUM(UserTransactions.price)","userTotalPrice")
                .where("UserTransactions.username = :username and UserTransactions.orgId = :orgId", {username : ctx.user,orgId:orgId})
                .getRawOne();
        //const checkUserRelationWithOrg = await  UserTransactions.find({where : {username:toUser?.username,orgId:orgId }})    
        console.log(userTotalPoints);
        console.log(userTotalPrice);
        const fromUserWalletId:any = fromUser?.walletId || 0;
        const toUserWalletId:any = walletId || 0;

        //Checking wallet toUser not fan of this club
        //if(checkUserRelationWithOrg.length != 0){
            // In sufficent Balance       
            if(userTotalPoints >= transferPoints){
                const fromTransactionData = {
                    username: ctx.user,
                    orgId: orgId, 
                    points: -transferPoints,
                    toWalletId : toUserWalletId,
                    price:-transferPoints * clubRate,
                    transactionType:"TRANSFER"
                };

                const toTransactionData = {
                    username: toUser?.username,
                    orgId: orgId,
                    points: transferPoints,
                    fromUsername : fromUser?.username,
                    fromWalletId : fromUserWalletId,
                    price:transferPoints * clubRate,
                    transactionType:"RECEIVED"
                };

                // Push notification
                if(toUser?.deviceId){
                    const pushObj:any = {
                        deviceId:toUser.deviceId,
                        title:"PointsVille",
                        message: "You recevied "+transferPoints+" from "+fromUser?.givenName
                    }
                    console.log("Push",pushObj);
                    postNotification(pushObj);
                }
                await UserTransactions.create(fromTransactionData).save();
                await UserTransactions.create(toTransactionData).save();
                await getConnection()
                .createQueryBuilder()
                .relation(User, "clubs")
                .of(toUser)
                .add(club)
                .then(() => { return true })
                .catch(() => { return true })
                return true
            }else{
                const error = new Error("In suffcient balance");
                throw error;
            }
        // }else{
        //     const error = new Error("Wallet user not fan of this club");
        //         throw error;
        // }

    }


     // User transfer point api
     @Authorized()
     @Mutation(() => Boolean)
     async redeem(
         @Arg("orgId", () => String) orgId:string,
         @Arg("redeemPoints", () => Int) redeemPoints:number,
         @Ctx() ctx: any
     ){
         const redeemUser:any = await User.findOne({where : {username : ctx.user}});
         const club  = await Club.findOne({where : {id:orgId}});
         const clubRate = club?.rate || 0;
         console.log(redeemUser?.walletId);
         const {userTotalPoints,userTotalPrice} = await createQueryBuilder(UserTransactions)
                 .select("SUM(UserTransactions.points)", "userTotalPoints")
                 .addSelect("SUM(UserTransactions.price)","userTotalPrice")
                 .where("UserTransactions.username = :username and UserTransactions.orgId = :orgId", {username : ctx.user,orgId:orgId})
                 .getRawOne();
            console.log(userTotalPrice);
            // In sufficent Balance       
            if(userTotalPoints >= redeemPoints){
                const fromTransactionData = {
                    username: ctx.user,
                    orgId: orgId, 
                    transactionType:"REDEEM",
                    points: -redeemPoints,
                    price: redeemPoints * clubRate
                };
                await UserTransactions.create(fromTransactionData).save();
                console.log("prev",redeemUser?.walletBalance)
                let userWalletBalance = redeemUser?.walletBalance + (redeemPoints * clubRate);
                        Object.assign(redeemUser,{walletBalance : userWalletBalance})
                redeemUser?.save();
                return true
            }else{
                const error = new Error("In suffcient balance");
                throw error;
            }
     }
}





@ObjectType()
class Wallet {
    
    @Field(() => Int!,{nullable:true})
    totalPoints?: number;
    @Field()
    totalAmount?: number;
    @Field()
    hunt?: number;
    @Field()
    promo?: number;
    @Field(() => Float!,{nullable:true})
    walletId ?:number;
    @Field()
    received?: number;
}


// @ObjectType()
// class PromoDetails {
//     @Field()
//     id: string;
//     @Field(() => String!, {nullable: true})
//     promoCode?: string;
//     @Field(() => String!, {nullable: true})
//     sponsorName?: string;
//     @Field(() => String!, {nullable: true})
//     sponsorLogo?: string;
//     @Field()
//     createdDate?: string;
//     @Field()
//     validityDate?: string;
// }

@ObjectType()
class Trans {
    @Field()
    id: string;
    @Field(() => String!,{nullable:true})
    username ?: string;
    @Field(() => String!,{nullable:true})
    userProfilePic: string;
    @Field(() => Float!,{nullable:true})
    walletId ?: number;
    @Field(() => String!, {nullable: true})
    orgId?: string;
    @Field(() => String!, {nullable: true})
    orgName?: string;
    @Field(() => String!, {nullable: true})
    orgLogo?: string;
    @Field (() => String!,{nullable:true})
    transactionType:string;
    @Field()
    orgRate?:number;
    @Field(() => Float!, {nullable: true})
    price?:number;
    @Field(() => String!, {nullable: true})
    sponsorName?: string;
    @Field(() => String!, {nullable: true})
    sponsorLogo?: string;
    @Field()
    transactionDate?: string;
    @Field(() => Float!, {nullable: true})
    points?: number;
    @Field(() => String!, {nullable: true})
    validity?: string;
    @Field(() => String!, {nullable: true})
    createdAt?: string;
    @Field(() => String!, {nullable: true})
    promoCode?: string;
}