import { handler } from "../../utils/decode-verify-jwt";
import { Resolver, Mutation, Arg, Ctx } from "type-graphql";

import { User } from "../../entity/User";
import { RegisterInput } from "./register/RegisterInput";
// import { isAuth } from "../middleware/isAuth";
// import { logger } from "../middleware/logger";
// import { sendEmail } from "../utils/sendEmail";
// import { createConfirmationUrl } from "../utils/createConfirmationUrl";

@Resolver()
export class whoamiResolver {

  @Mutation(() => User)
  async whoami(@Arg("data")
  {
    email,
    givenName,
    phoneNumber,
    deviceId
  }: RegisterInput, @Ctx() ctx: any): Promise<any> {

    const auth = await handler({ token: ctx.req.headers.authorization });
    if (auth.isValid) {
      let u: User | undefined = await User.findOne({ username: auth.userName });
      if (!!u) {
        //device Token Update
        console.log(u);
        const data = {deviceId:deviceId}
        Object.assign(u,data);
        u.save();
        return u;

      } else {
        const startOrderNumber = 1001999900101001;
        let highestOrderNumber = await User
        .createQueryBuilder('user')
        .select('MAX(user.walletId)', 'walletId')
        .getRawOne();
        const newWalletNumber = highestOrderNumber.walletId < startOrderNumber ? startOrderNumber : parseInt(highestOrderNumber.walletId) + 1;
        

        const user: User | undefined  = await User.create({
          givenName,
          email,
          username: auth.userName,
          phoneNumber,
          permissions: "[]",
          isActive: true,
          walletId: newWalletNumber,
          deviceId
        }).save();
        
        return user

      }
    }
  }
}
