import { handler } from "../../utils/decode-verify-jwt";
import { Resolver, Query, Ctx } from "type-graphql";

import { User } from "../../entity/User";
import { MyContext } from "../../types/MyContext";

@Resolver()
export class MeResolver {
  @Query(() => User, { nullable: true, complexity: 5 })
  async me(@Ctx() ctx: MyContext): Promise<User | any> {
    const auth = await handler({ token: ctx.req.headers.authorization });
    console.log()
    if (auth.isValid) {

      let u: User | undefined = await User.findOne({ username: auth.userName });
      if (!!u) {
        return u;
      } else {
        throw new Error("not authenticated");
      }
    } else {
      throw new Error("not authenticated");
    }
  }
}
