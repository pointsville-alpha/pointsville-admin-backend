import  { Resolver, Query, Mutation, Arg, Int,Float, Field, Authorized,ObjectType} from 'type-graphql';
import { createQueryBuilder } from "typeorm";

import {UpdatedUserInput } from "./register/RegisterInput";
import { User } from "../../entity/User";
import {UserTransactions} from '../../entity/UserTransactions';

@Resolver()
export class UserResolver{ 

    // Update
    @Authorized("ADMIN")
    @Mutation( () => User)
    async updateUserDetails(
        @Arg("username",() => String) username:string,
        @Arg("data") data: UpdatedUserInput
    ){
        const updaterUser = await User.findOne({ where: { username } });
        if (!updaterUser) throw new Error("User not found!");
        Object.assign(updaterUser, data);
        await updaterUser.save();
        return updaterUser;
    }


  @Authorized("USER")
    @Mutation( () => User)
    async updateUserDetailsMobile(
        //@Ctx() ctx: any,
        @Arg("data") data: UpdatedUserInput
    ){
        const updaterUser = await User.findOne({ where: { username:"39b637b0-24bc-41ff-a66e-a25021f82bbe" } });
        if (!updaterUser) throw new Error("User not found!");
        Object.assign(updaterUser, data);
        await updaterUser.save();
        return updaterUser;
    }

    //View
    @Authorized("ADMIN")
    @Query( () => [userDetails])
     async getUser(){
        const users = await User.find({where:{userRole:"USER"}});
        //console.log(users);
        const userResult:any = users.map(async (user: any) => {    
        const {walletPoint} = await createQueryBuilder(UserTransactions)
          .select("SUM(UserTransactions.points)", "walletPoint")
          .where("UserTransactions.username = :username", {username: user.username})
          .getRawOne();
          console.log(walletPoint);
          return {
            id : user.id,
            givenName:user.givenName,
            email:user.email,
            phoneNumber:user.phoneNumber,
            isActive:user.isActive,
            userPrice:walletPoint | 0,
            walletBalance:user.walletBalance | 0,
            walletId:user.walletId,
            profileImage : user.profileImage,
            username:user.username,
            createdAt : user.createdAt

          }

      })
        return userResult;
    }

    //view with id

    @Authorized("ADMIN")
    @Query(() => User)
    async getUserWithId(@Arg("userId", () => String) userId: string) {
      const user = await User.findOne({ username: userId });
      return user;
    }
} 


@ObjectType()
class userDetails {
    
    @Field(() => String!, {nullable: true})
    id?: string;

    @Field(() => String!, {nullable: true})
    givenName ?: string;
    
    @Field(() => String!, {nullable: true})
    username?: string;

    @Field(() => String!, {nullable: true})
    email?: string;

    @Field(() => String!, {nullable: true})
    phoneNumber?: string;

    @Field(() => String!, {nullable: true})
    profileImage?: string;

    @Field(() => Float,{nullable : true})
    walletId ?: number;

    @Field(() => Int!,{nullable : true})
    walletBalance ?: number;
    
    @Field(() => Int!,{nullable : true})
    userPrice ?: number;


    @Field(() => Int!,{nullable : true})
    walletPoints ?: number;
    
    @Field(() => Int!,{nullable : true})
    fanCount ?: number;

    @Field()
    isActive?:boolean

    @Field()
    createdAt :string;

}