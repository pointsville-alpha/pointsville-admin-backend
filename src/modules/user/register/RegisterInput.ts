import { Length, IsEmail } from "class-validator";
import { Field, InputType } from "type-graphql";
// import { PasswordMixin } from "../../shared/PasswordInput";

@InputType()
export class RegisterInput {
  @Field()
  @Length(1, 255)
  givenName: string;

  @Field()
  @Length(1, 255)
  phoneNumber: string;

  @Field()
  @IsEmail()
  email: string;
  
  @Field({nullable: true })
  deviceId ?: string;


  @Field({nullable:true})
  availedFirstStatus ?: Boolean;
  // @Field({nullable: true })
  // deviceId ?: string;

}


@InputType()
export class UpdatedUserInput {
  @Field({ nullable: true })
  @Length(1, 255)
  givenName?: string;

  @Field({ nullable: true })
  @Length(1, 255)
  phoneNumber ?: string;

  @Field({ nullable: true })
  @Length(1, 255)
  username ?: string;

  @Field({nullable: true })
  walletId ?: number

  @Field({nullable: true })
  deviceId ?: string;

  @Field({ nullable: true })
  @IsEmail()
  email ?: string;

  @Field({nullable:true})
  dob  ?: Date;

  
  @Field({nullable:true})
  address ?: string;


  @Field({nullable:true})
  isActive ?:Boolean;

  @Field({nullable:true})
  availedFirstStatus ?:Boolean;

}
