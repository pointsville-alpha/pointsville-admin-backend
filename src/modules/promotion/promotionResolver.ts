import { UserTransactions } from '../../entity/UserTransactions';
import  { Resolver, Query, Mutation, Arg, Int, InputType,ObjectType, Field, Authorized, Ctx} from 'type-graphql';
import {Promotion} from '../../entity/Promotion';
import { createQueryBuilder ,getRepository} from "typeorm";

import {Email} from '../email/sendEmail';
//import {postNotification} from '../firebase/firebase';
import { User } from '../../entity/User';
import { getConnection } from 'typeorm';
import { Club } from '../../entity/Club';
import { Sponsor } from "../../entity/Sponsor";
import { postNotification } from '../firebase/firebase';

@InputType()
class NewPromoInput {
    @Field( {nullable: true })
    promoCodePrefix: string;

    @Field(()=>Int, {nullable: true })
    promoCodePoints : number;

    @Field({nullable: true })
    validity: string;

    @Field (() => Int,{nullable:true})
    validityDay:number;


    @Field({nullable: true })
    sponsorId: string;

    @Field({nullable: true })
    sponsorLogo ?: string;

    @Field({nullable: true })
    orgId: string;

    @Field({nullable: true })
    promoType: string;

    @Field(() => Int,{nullable: true })
    pointsAvailable: number;

    @Field(() => Int,{nullable: true })
    promoTotalCount: number;

    @Field(() => Int,{nullable: true })
    noPointsPerPromo : number;

    @Field({nullable: true })
    promoMessage ?: string;

    @Field({nullable: true })
    promoTitle ?: string;

    @Field({nullable: true })
    promoSentToMail ?: string;

    @Field({nullable: true })
    promoSentTOClub ?: string;

}


@InputType()
class UpdatePromoInput {

    @Field( {nullable: true })
    promoCodePrefix ?: string;

    @Field(()=>Int, {nullable: true })
    promoCodePoints ?: number;

    @Field({nullable: true })
    validity ?: string;


    @Field (() => Int,{nullable:true})
    validityDay:number;

    @Field({nullable: true })
    sponsorId : string;

    @Field({nullable: true })
    sponsorLogo ?: string;

    @Field({nullable: true })
    orgId: string;

    @Field({nullable: true })
    promoType ?: string;

    @Field(() => Int,{nullable: true })
    pointsAvailable ?: number;

    @Field(() => Int,{nullable: true })
    promoTotalCount ?: number;

    @Field(() => Int,{nullable: true })
    noPointsPerPromo ?: number;

    @Field({nullable: true })
    promoMessage ?: string;

    @Field({nullable: true })
    promoTitle ?: string;

    @Field({nullable: true })
    promoSentToMail ?: string;

    @Field({nullable: true })
    promoSentTOClub ?: string;

}
@Resolver()
export class PromotionResolver{ 
   //Create
   @Authorized('ADMIN')
   @Mutation( () => Promotion)
    async createPromo(
        @Arg("data") data:NewPromoInput
        ){
        let promo = await Promotion.find({where : {promoCode : data.promoCodePrefix+data.promoCodePoints}});
        if (!promo.length) {
            const promotion = await Promotion.create({...data, promoCode: data.promoCodePrefix+data.promoCodePoints }).save();
            const sponsor = await Sponsor.findOne({where: {id: data.sponsorId}}); 
            const totalPoints = data.promoCodePoints * data.noPointsPerPromo;
            if (sponsor){
                if(sponsor.availablePoints >= totalPoints ){
                    //const sponsorTotal = sponsor.availablePoints - totalPoints;
                    const data = { promotions : sponsor.promotions + 1}
                    Object.assign(sponsor,data);
                    await sponsor.save();
                }
                else{
                    const error = new Error("Please check available points in sponsor");
                    throw error;
                }
            }
            
            if(data.promoType == "Email"){
                let email = new Email()
                var to  = data.promoSentToMail
                var subject = "Promotion"
                var sponsorLogo = data.sponsorLogo || "https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg";
                var template = `<!DOCTYPE><html ><head><title>PointsVille Promotion Email</title><meta name="viewport" content="width=device-width, initial-scale=1.0"/></head><body style="margin: 0; padding: 0;"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td style="padding: 10px 0 30px 0;"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" border-collapse: collapse;"> <tr style="border-radius: 30px;background-image: url(http://13.234.38.148/static/media/promo_email_preview.8b4dd394.svg);background-repeat: no-repeat;background-size: 100%;background-color:blue;"> <td align="center" style=""> <img src="`+ sponsorLogo+`" alt="Creating Email Magic" width="50" height="50" style="display: block;border-radius: 30px;" /> </td> <td align="center" style="padding:20px;color: #fff;display: block;text-align: center;font-size: 18px;line-height: 25px;font-family: 'Graphik Regular';"> <label>`+data.promoTitle+`</label> </td> </tr> <tr> <td bgcolor="#ffffff" colspan="2" style="padding: 10px 20px;color:#757575;font-size: 12px;"> <p>`+data.promoMessage+`</p> </td> </tr> <tr> <td colspan="2" style="text-align: center;padding-top:20px" > <label style="display: inline-block;border: 1px dashed rgba(43,46,76,.5);border-radius: 7px;padding: 15px 20px;margin-bottom: 10px;font-family: 'Graphik Medium';font-size: 18px;color: #2b2e4c;line-height: 22px;text-align: left;font-weight: 400;">`+data.promoCodePrefix+data.promoCodePoints+`</label> </td> </tr> <tr> <td colspan="2" style="text-align: center;"> <label style="font-family: 'Graphik Regular';display:block;line-height: 22px;text-align: center;font-size: 12px;color: #afafaf;" >Valid till `+data.validity+`</label> </td> </tr> </table> </td> </tr> </table></body></html>`
                email.sendEmail(to,subject,template);
            }else if(data.promoType == "Notification"){
                const  club = await Club.find();

                club.map(async (clubdetails:any) => {
                    console.log(clubdetails.id);
                })

                if (data.promoSentTOClub == 'all') {
                    const userTransaction: any = await getRepository(UserTransactions)
                    .createQueryBuilder("usertransaction")
                    .distinctOn(["usertransaction.username"])
                    .orderBy("usertransaction.username")
                    .getMany();
                    console.log(userTransaction);
                    userTransaction.map(async(transaction:any) => {
                        const user = await User.findOne({where :{username:transaction.username}});
                        if(user?.deviceId){
                            const pushObj:any = {
                                deviceId:user.deviceId,
                                title:"PointsVille",
                                message: data.promoCodePrefix + data.promoCodePoints
                            }
                            console.log("Push",pushObj);
                            postNotification(pushObj);
                        }
                    });
                } 
            }
            return promotion;
          } else {
            const error = new Error("Promo already exists, Try new one");
            throw error;
        }
    }

    // Update
    @Authorized("ADMIN")
    @Mutation( () => Promotion)
    async updatePromo(
        @Arg("id",() => String) id:string,
        @Arg("data") data: UpdatePromoInput
    ){
        const updatedPromo = await Promotion.findOne({ where: { id } });
        if (!updatedPromo) throw new Error("Promo not found!");
        Object.assign(updatedPromo, data);
        await updatedPromo.save();
        return updatedPromo;
    }

    //Delete
    @Authorized("ADMIN")
    @Mutation (() => Boolean)
    async deletePromo(
        @Arg("id", () => String) id:string
    ){
        await Promotion.delete({id});
        return true;
    }

    //View
    @Authorized()
    @Query(() => [PromotionsWith])
     async getPromotions(){
        const promotions = await createQueryBuilder(Promotion)
        .orderBy("Promotion.createdAt", "DESC")
        .getMany();
        console.log(promotions);
        const promo = promotions.map(async (promo: any) => {

            const org = await Club.findOne({where: {id: promo.orgId}});
            const sponsor = await Sponsor.findOne({where: {id: promo.sponsorId}});
            return {
                id: promo.id,
                orgName: org?.name,
                orgLogo: org?.logoUrl,
                sponsorName: sponsor?.sponsorName,
                sponsorLogo: sponsor?.sponsorLogo,
                availablePoints: promo.pointsAvailable,
                validityDay: promo.validityDay
            }

        })
        
        return promo;
    }

    //Viewwith Single 
    // @Authorized()
    // @Query( () => Promotion)
    //  async getPromoWithId(
    //     @Arg("promoId",() => String) promoId:string
    //  ){
    //     const promoWithId = await Promotion.findOne({promoId : promoId });
    //     if (!promoWithId) {
    //         throw new Error("Invalid recipe ID");
    //       }
    //       return promoWithId;
    // }


    @Authorized()
    @Query(() => Promotion)
    async getPromoWithId(@Arg("id", () => String) id: string) {
      const promotion = await Promotion.findOne({ id: id });
      return promotion;
    }

    @Authorized()
    @Query(() => [PromotionsAll])
    async getPromoWithOrgId(@Arg("orgId", () => String) orgId: string) {
      const promotion = await Promotion.find({where:{ orgId: orgId }});
      const promoDetails:any = promotion.map(async(promoDetail:any) => {
        return{
            id:promoDetail.id,
            promoCodePrefix : promoDetail.promoCodePrefix,
            promoCodePoints : promoDetail.promoCodePoints,
            promoType : promoDetail.promoType,
            validity : promoDetail.validity,
            promoTotalCount : promoDetail.promoTotalCount,
            sponsorId : promoDetail.sponsorId,
            pointsAvailable : promoDetail.pointsAvailable,
            noPointsPerPromo : promoDetail.noPointsPerPromo,
            sponsorLogo : promoDetail.sponsorLogo,
            createdAt : promoDetail.createdAt
        }
        })      
        return promoDetails;
    }

    @Authorized()
    @Query(() => [Promotion])
    async getPromoWithSponsorId(@Arg("sponsorId", () => String) sponsorId: string) {
      const promotion = await Promotion.find({where:{ sponsorId: sponsorId }});
      return promotion;
    }

    //@Authorized()
    @Mutation(() => Boolean)
    async availPromo(
        @Arg("promoCode", () => String) promoCode:string,
        @Ctx() ctx: any
    ){
        console.log(promoCode);
        const promotion:any = await Promotion.findOne({where:{ promoCode: promoCode }});
        console.log(promotion);
        if(promotion){
        const userTransaction = await UserTransactions.find({where:{ promoId: promotion.id, username: ctx.user}});
        
        if (userTransaction.length === 0) {
            const club = await Club.findOne(promotion.orgId)
            const orgRate = club?.rate || 0;
            const transactionData = {
                username: ctx.user,
                orgId: promotion.orgId, 
                sponsorId: promotion.sponsorId, 
                promoType: promotion.promoType, 
                promoId: promotion.id, 
                promoCode: promotion.promoCode, 
                points: promotion.promoCodePoints, 
                price  : promotion.promoCodePoints*orgRate,
                transactionType:"PROMO"
            };


            // Update org points
            const clubDetail:any = await Club.findOne({where : { id:promotion.orgId}});
            if(club){
                const clubTotal = clubDetail.availablePoints - promotion.promoCodePoints;
                const data = { availablePoints : clubTotal}
                Object.assign(clubDetail,data);
                await clubDetail.save();
            }
            // update available promocodes
            console.log("promo",promotion.pointsAvailable);
            if(parseInt(promotion.pointsAvailable) === 0){
                const availablePromo = promotion.pointsAvailable - 1;
                const data = { pointsAvailable : availablePromo}
                Object.assign(promotion,data);
                await promotion.save();
            }else{
                //const error = new Error("Oops, This promo not available");
                //throw error;
            }

            // Update user points
            // Fan Count in org
            // sponsor points reduce
            const sponsor = await Sponsor.findOne({where: {id: promotion.sponsorId}}); 
            if(sponsor){
                const sponsorTotal = sponsor.availablePoints - promotion.promoCodePoints;
                const data = { availablePoints : sponsorTotal}
                Object.assign(sponsor,data);
                await sponsor.save();
            }


            const user = await User.findOne({where: {username: ctx.user}})
            await UserTransactions.create(transactionData).save();
            await getConnection()
                .createQueryBuilder()
                .relation(User, "clubs")
                .of(user)
                .add(club)
                .then(() => { return true })
                .catch(() => { return true })
            return true
        } else {
            return false
        }
        }else{
            const error = new Error("Oops, This promo not available");
            throw error;
        }
    }

} 

@ObjectType()
class PromotionsAll{
    @Field()
    id: string;
    @Field( {nullable: true })
    promoCodePrefix ?: string;

    @Field(()=>Int !, {nullable: true })
    promoCodePoints ?: number;

    @Field(() => String!, {nullable: true})
    validity ?: string;


    @Field (() => Int!,{nullable:true})
    validityDay:number;

    @Field(() => String!, {nullable: true})
    sponsorId : string;

    @Field(() => String!, {nullable: true})
    sponsorLogo ?: string;

    @Field(() => String!, {nullable: true})
    orgId: string;

    @Field(() => String!, {nullable: true})
    promoType ?: string;

    @Field(() => Int!,{nullable: true })
    pointsAvailable ?: number;

    @Field(() => Int!,{nullable: true })
    promoTotalCount ?: number;

    @Field(() => Int!,{nullable: true })
    noPointsPerPromo ?: number;

    @Field(() => String!,{nullable: true })
    createdAt ?: string;

}


@ObjectType()
class PromotionsWith {
    @Field()
    id: string;
    @Field(() => String!, {nullable: true})
    orgName?: string;
    @Field(() => String!, {nullable: true})
    orgLogo?: string;
    @Field(() => String!, {nullable: true})
    sponsorName?: string;
    @Field(() => String!, {nullable: true})
    sponsorLogo?: string;
    @Field(() => String!, {nullable: true})
    availablePoints?: number;
    @Field(() => String!, {nullable: true})
    validityDay?: number;

}