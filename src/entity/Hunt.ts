import { MaxLength } from "class-validator";
import { Field, Float, ID, Int, ObjectType } from "type-graphql";
import {
    BaseEntity,
    BeforeInsert,
    BeforeUpdate,
    Column,
    CreateDateColumn,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { Point } from "./Point";

@ObjectType()
@Entity()
export class Hunt extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Field()
    @MaxLength(30)
    @Column({ nullable: true })
    huntCodePrefix: string;

    @Field(() => Int)
    @MaxLength(30)
    @Column({ nullable: true })
    huntCodePoints?: number;

    @Field()
    @Column({ nullable: true })
    huntCode?: string;

    @Field()
    @MaxLength(30)
    @Column({ nullable: true })
    validity: string;

    @Field(() => Int)
    @Column({ nullable: true, default: "0" })
    validityDay: number;

    @Field(() => String)
    @Column({ nullable: true })
    sponsorId: string;

    @Field()
    @Column({ nullable: true, default: "" })
    sponsorLogo: string;

    @Field()
    @Column({ nullable: true })
    orgId: string;

    @Field(() => Int, { nullable: true })
    @Column({ nullable: true })
    pointsAvailable: number;


    @Field(() => Int)
    @Column({ nullable: true })
    huntTotalPoints: number;

    @Field(() => Float)
    @Column({ nullable: true })
    noPointsPerHunt: number;

    @Field()
    @Column({ nullable: true })
    locationName: string;

    @Field(() => String)
    @Column({type: 'point', nullable: true})
    location: string | object;

    @Field(() => Float)
    @Column("decimal", { nullable: true })
    mainLat: number;

    @Field(() => Float)
    @Column("decimal", { nullable: true })
    mainLng: number;

    @Field(() => [Point])
    @OneToMany(() => Point, point => point.hunt)
    points: Point[];

    @Field(() => Boolean, {nullable: true})
    @Column({ nullable: true })
    isActive: boolean;

    
    @Field(() => Date, {nullable: true})
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public createdAt: Date;

    /**
     * DB last update time.
     */
    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updatedAt: Date;

    @BeforeInsert()
    updateDateCreation() {
        this.createdAt = new Date();
    }

    @BeforeUpdate()
    updateDateUpdate() {
        this.updatedAt = new Date();
    }

}

// interface IPoint {
//     x: number,
//     y: number
// }
