import { Field,ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from "typeorm";
@ObjectType()
@Entity()
export class GameSchedule extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  guid: string;

  @Field()
  @Column({ nullable: true })
  game_data: string;

  
  @Field()
  @Column({ nullable: true })
  orgcode: string;

  
}