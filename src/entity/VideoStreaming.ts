import { Field,ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn
} from "typeorm";
@ObjectType()
@Entity()
export class VideoStreaming extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  guid: string;

  @Field()
  @Column({ nullable: true })
  videoUrl: string;

  
  @Field()
  @Column({ nullable: true })
  orgCode: string;

  @Field()
  @Column({ nullable: true })
  videoTitle: string;
  
  @Field()
  @Column({ nullable: true })
  videoDescription: string;

  @Field()
  @Column({ nullable: true })
  videoImg: string;
  
}