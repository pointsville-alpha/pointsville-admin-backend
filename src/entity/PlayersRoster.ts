import { Field,ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
@ObjectType()
@Entity()
export class PlayRoster extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  guid: string;

  @Field()
  @Column({ nullable: true })
  player_name: string;

  @Field()
  @Column({ nullable: true })
  player_no: number;

  @Field()
  @Column({ nullable: true })
  player_field: string;


  @Field()
  @Column({ nullable: true })
  player_bt: string;

  @Field()
  @Column({ nullable: true })
  player_ht: string;

  @Field()
  @Column({ nullable: true })
  player_wt: string;


  @Field()
  @Column({ nullable: true })
  player_dob: string;


  @Field()
  @Column({ nullable: true })
  orgcode: string;


  @Field()
  @Column({ nullable: true })
  playerUrl: string;


  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }
}