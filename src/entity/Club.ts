import { MaxLength, Length } from "class-validator";
import { Field, Int,Float, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { Sponsor } from "./Sponsor";
import { User } from "./User";
// import { MyContext } from "../types/MyContext";
// import { Author } from "./Author";
// import { Sponsor } from "./Sponsor";

@ObjectType()
@Entity()
export class Club extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field()
  @MaxLength(30)
  @Column()
  name: string;


  @Field()
  @MaxLength(30)
  @Column()
  orgCode: string;

  @Field(() => Float,{nullable:true})
  @Column({type:"float",nullable:true})
  rate?: number;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  logoUrl?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  rosterApi?: string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  scheduleApi?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  videoApi?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  eventsApi?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  newsApi?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  facebook?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  instagram?: string;

  @Field(() => String!, {nullable:true})
  @Length(5, 255)
  @Column({ nullable: true })
  twitter?: string;

  @Field(() => Int)
  @Column({default:0})
  fanCount?: number;

  @Field()
  @Column({default:true})
  isActive?:boolean

  @Field(() => Int)
  @Column({default:0})
  distributionPoints?: number;

  @Field(() => Int)
  @Column({default:0})
  availablePoints: number;

  @Field(() => Int!, {nullable : true})
  @Column({default:0})
  totalPoints : number;

  @ManyToMany(() => User, user => user.clubs, {cascade: false})
  users: User[];

  // @OneToMany(() => Sponsor, sponsor => sponsor.club)
  // sponsors: Sponsor[];

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }

  @OneToMany(() => Sponsor, sponsor => sponsor.club)
  sponsors: Sponsor[];

  // @Field(() => [AuthorBook])
  // @OneToMany(() => AuthorBook, ab => ab.book)
  // authorConnection: Promise<AuthorBook[]>;

  // @Field(() => [Author])
  // async authors(@Ctx() { authorsLoader }: MyContext): Promise<Author[]> {
  //   return authorsLoader.load(this.id);
  // }
}
