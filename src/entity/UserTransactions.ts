import { Field, ID, ObjectType ,Int,Float} from "type-graphql";
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  
} from "typeorm";

// import {User} from '../entity/User';
@ObjectType()
@Entity()
export class UserTransactions extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  username?: string;
  
  @Field(() => String!, {nullable:true})
  @Column({ nullable: false, type: "uuid" })
  orgId?: string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true, type: "uuid" })
  sponsorId?: string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  promoId?:string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  promoCode?:string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  promoType?:string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  fromUsername?: string;

  @Field(() => Int)
  @Column({ nullable: true })
  points?: number;

  @Field()
  @Column({
    type: "enum",
    enum: ["PROMO","HUNT","TRANSFER","REDEEM","RECEIVED"],
    default: "PROMO"
  })
  transactionType: string;

  @Field(() => Float!)
  @Column({ nullable: true,type:"float",default:0 })
  price?: number;

  @Field(() => Float!, {nullable:true})
  @Column({type: "bigint",nullable: true})
  toWalletId?: number;

  @Field(() => Float!, {nullable:true})
  @Column({type: "bigint",nullable: true})
  fromWalletId?: number;
  
  // @ManyToMany(type => User, user => user.transactions)
  //   users: User[];


  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }

}
