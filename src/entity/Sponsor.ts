import { Field, ID, ObjectType,Int } from "type-graphql";
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { Club } from "./Club";
// import { MyContext } from "../types/MyContext";
// import { Club } from "./Club";

@ObjectType()
@Entity()
export class Sponsor extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field()
  @Column({ nullable: true })
  sponsorName: string;
  
  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  sponsorLogo: string;

  @Field()
  @Column({ nullable: true })
  pointRequest:number;

  @Field(() => Int!, {nullable:true})
  @Column({ nullable: true })
  availablePoints:number;

  @Field(() => Int!, {nullable:true})
  @Column({ nullable: true,default:0 })
  promotions:number;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  contactName:string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  contactEmail : string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  contactPhone : string;


  @Field(() => String!, {nullable:true})
  @Column({ nullable: true })
  contactAddress ?: string;
  
  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }

  @Field()
  @Column({ nullable: true })
  clubId:string;
  
  @ManyToOne(() => Club, club => club.sponsors)
  club: Club;

  // @ManyToOne(() => Club, club => club.sponsors)
  //    club ?: Club;


  // @ManyToOne(() => Club, club => club.sponsors)
  //   club: Club;

  // @OneToMany(type => Photo, photo => photo.user)
  //   photos: Photo[];

  // @Field(() => [AuthorBook])
  // @OneToMany(() => AuthorBook, ab => ab.book)
  // authorConnection: Promise<AuthorBook[]>;

  // @Field(() => [Author])
  // async authors(@Ctx() { authorsLoader }: MyContext): Promise<Author[]> {
  //   return authorsLoader.load(this.id);
  // }
}
