import { MaxLength} from "class-validator";
import { Field, Float, ID, Int, ObjectType } from "type-graphql";
import {
    BaseEntity,
    BeforeInsert,
    BeforeUpdate,
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
  } from "typeorm";

@ObjectType()
@Entity()
export class Promotion extends BaseEntity {
@Field(() => ID)
@PrimaryGeneratedColumn("uuid")
id: string;

@Field()
@MaxLength(30)
@Column({ nullable: true })
promoCodePrefix: string;

@Field(() => Int)
@MaxLength(30)
@Column({ nullable: true })
promoCodePoints?: number;

@Field()
@Column({ nullable: true })
promoCode?: string;

@Field()
@MaxLength(30)
@Column({nullable: true })
validity: string;

@Field(() => Int)
@Column({nullable: true,default:"0" })
validityDay: number;

@Field()
@Column({ nullable: true, type: "uuid" })
sponsorId: string;


@Field()
@Column({ nullable: true,default:"" })
sponsorLogo: string;

@Field()
@Column({ nullable: true, type: "uuid" })
orgId: string;


@Field()
@MaxLength(30)
@Column({ nullable: true })
promoType: string;

@Field(() => Int)
@Column({ nullable: true })
pointsAvailable: number;


@Field(() => Int)
@Column({ nullable: true })
promoTotalCount: number;


@Field(() => Float)
@Column({ nullable: true })
noPointsPerPromo: number;

@Field()
@Column({ nullable: true })
promoMessage: string;

@Field()
@Column({ nullable: true })
promoTitle: string;

@Field()
@Column({ nullable: true })
promoSentToMail?: string ;


@Field(() => String!, {nullable:true})
@Column({ nullable: true })
promoSentTOClub ?: string;

@CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }


}

