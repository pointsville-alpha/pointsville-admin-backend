import { Field, Float, ID, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Hunt } from "./Hunt";
import { User } from "./User";

@ObjectType()
class IPoint {
    @Field(() => Float)
    x: number;

    @Field(() => Float)
    y: number
}

@ObjectType()
@Entity()
export class Point extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Field(() => Float)
    @Column({nullable: true})
    pointsValue: number;

    @Field(() => Float)
    @Column("decimal", {nullable: true})
    lat: number;

    @Field(() => Float)
    @Column("decimal", {nullable: true})
    lng: number;

    @Field(() => IPoint)
    @Column({type: 'point', nullable: true})
    location: string | object;

    // @Field(() => Int)
    // @Column({type: "point", nullable: true, transformer: {
    //     from: p => p, // comes from database in format we need already,
    //     to: p => `${p.x},${p.y}` // { x: 1, y: 2 } -> '1,2'
    // }})
    // location: IPoint;
    
    @ManyToOne(() => Hunt, hunt => hunt.points)
    hunt: Hunt;

    @ManyToMany(() => User, user => user.points)
    users: User[];

}

