import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, CreateDateColumn, UpdateDateColumn, BeforeInsert, BeforeUpdate, PrimaryColumn, ManyToMany, JoinTable } from "typeorm";
import { ObjectType, Field, ID, Root ,Float} from "type-graphql";
import { Club } from "./Club";
import { Point } from "./Point";
// import {UserTransaction} from './UserTransactions';

@ObjectType()
@Entity()
export class User extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  givenName: string;

  @Field()
  @Column("text", { unique: true })
  email: string;

  @Field()
  @Column("text")
  phoneNumber: string;

  @Field(() => Float!,{nullable:true})
  @Column({type: "bigint",nullable: true})
  walletId: number;

  @Field(() => Float!,{nullable:true})
  @Column("decimal",{nullable: true})
  walletBalance ?: number;

  @Field(() => String!,{nullable:true})
  @Column({nullable: true})
  profilePicture: string;

  @Field()
  @Column({
    type: "enum",
    enum: ["ADMIN", "USER"],
    default: "USER"
  })
  userRole: string;

  @Field(() => String!,{nullable:true})
  @Column({nullable: true})
  permissions: string;

  @Field()
  @Column({type: "bool", default: true })
  isActive: boolean;

  @Field({nullable:true})
  @Column( {type:"bool",default:false}) 
  availedFirstStatus:boolean;

  @Field()
  @PrimaryColumn()
  username : string;

  @Field({nullable:true})
  @Column({default:new Date()})
  dob ?: Date;

  @Field(() => String,{nullable:true})
  @Column({ nullable:true,default:"" })
  address ?: string;

  @Field(() => String!, {nullable:true})
  @Column({ nullable:true,default:"" })
  deviceId ?:string  

  @Field({ complexity: 3 })
  name(@Root() parent: User): string {
    return `${parent.givenName}`;
  }

  @ManyToMany(() => Club, club => club.users, {cascade: false})
  @JoinTable()
  clubs?: Club[];

  @ManyToMany(() => Point, point => point.users)
  @JoinTable()
  points: Point[];

  // @OneToMany(() => UserTransaction, transaction => transaction.users,{cascade: false})
  // @JoinTable()
  // transactions: UserTransaction[];

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  /**
   * DB last update time.
   */
  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;

  @BeforeInsert()
  updateDateCreation() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDateUpdate() {
    this.updatedAt = new Date();
  }
}
